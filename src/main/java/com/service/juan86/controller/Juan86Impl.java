package com.service.juan86.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.apache.servicecomb.provider.pojo.RpcSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CsePojoDemoCodegen", date = "2018-08-06T01:49:23.513Z")

@RpcSchema(schemaId = "juan86")
public class Juan86Impl implements Juan86{

    @Autowired
    private Juan86Delegate juan86Delegate;


    public String helloworld(String name) {

        return juan86Delegate.helloworld(name);
    }

}
