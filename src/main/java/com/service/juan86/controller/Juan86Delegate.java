package com.service.juan86.controller;

import org.springframework.stereotype.Component;


@Component
public class Juan86Delegate implements Juan86 {

    public String helloworld(String name){

        // Do Some Magic Here!
        return name;
    }
}
