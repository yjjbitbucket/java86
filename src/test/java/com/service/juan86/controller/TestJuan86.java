package com.service.juan86.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestJuan86 {

    Juan86Delegate juan86Delegate = new Juan86Delegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = juan86Delegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}
